val ScalatraVersion = "2.6.2"

organization := "mahidol.muic.bigdata"

name := "project0"

version := "0.1.0-SNAPSHOT"

scalaVersion := "2.12.3"

resolvers ++= Seq(
  Classpaths.typesafeReleases,
  "Hyperreal Repository" at "https://dl.bintray.com/edadma/maven"
)
val opRabbitVersion = "2.1.0"

libraryDependencies ++= Seq(
  "com.spingo" %% "op-rabbit-core"        % opRabbitVersion,
  "com.spingo" %% "op-rabbit-json4s"      % opRabbitVersion,
  "com.spingo" %% "op-rabbit-akka-stream" % opRabbitVersion
)
libraryDependencies += "com.newmotion" %% "akka-rabbitmq" % "5.0.0"
libraryDependencies ++= Seq(
  "org.scalatra" %% "scalatra" % ScalatraVersion,
  "org.scalatra" %% "scalatra-scalatest" % ScalatraVersion % "test",
  "ch.qos.logback" % "logback-classic" % "1.1.5" % "runtime",
  "org.eclipse.jetty" % "jetty-webapp" % "9.2.15.v20160210" % "container;compile",
  "javax.servlet" % "javax.servlet-api" % "3.1.0" % "provided",
  "xyz.hyperreal" %% "b-tree" % "0.3",
  "org.json4s"   %% "json4s-jackson" % "3.5.2",
  "com.roundeights" %% "hasher" % "1.2.0",
  "org.scalatra" %% "scalatra-json" % ScalatraVersion
)
// https://mvnrepository.com/artifact/commons-validator/commons-validator
libraryDependencies += "commons-validator" % "commons-validator" % "1.4.0"
// https://mvnrepository.com/artifact/org.json4s/json4s-jackson
libraryDependencies += "org.json4s" %% "json4s-jackson" % "3.5.0"
// https://mvnrepository.com/artifact/org.apache.httpcomponents/httpclient
libraryDependencies += "org.apache.httpcomponents" % "httpclient" % "4.5"
// https://mvnrepository.com/artifact/commons-io/commons-io
libraryDependencies += "commons-io" % "commons-io" % "2.6"

assemblyJarName in assembly := "project0.jar"
test in assembly := {}


enablePlugins(SbtTwirl)
enablePlugins(ScalatraPlugin)
enablePlugins(JettyPlugin)

containerPort in Jetty := 8090
