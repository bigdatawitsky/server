# Pull base image
FROM openjdk:8

COPY target/scala-2.12/project0.jar .

RUN ["java", "-version"]

CMD ["java", "-jar", "project0.jar"]