import mahidol.muic.bigdata.global.WorkerPoll
import org.eclipse.jetty.server.Server
import org.eclipse.jetty.webapp.WebAppContext
import org.scalatra.servlet.ScalatraListener
import mahidol.muic.bigdata.servlet.{JobDispatcherServlet, WorkerCommunicationServlet}

import scala.io.Source

object JettyRunner extends App{
  def run(): Unit = {
    def readProperties(): Map[String,String] = {
      val properties = Source.fromResource("general.properties").getLines
      properties.map(line => line.split("=")).map((arr) => arr(0) -> arr(1)).toMap
    }
    val properties = readProperties()
    val port = properties.get("port")
    if (port.isEmpty)
      throw new Exception("Properties malformed")
    val server = new Server(port.get.toInt)
    val context = new WebAppContext()
    context setContextPath "/"
    context setResourceBase "src/main/resources"
    context.addEventListener(new ScalatraListener)

    context.addServlet(classOf[JobDispatcherServlet],"/job/*")
    context.addServlet(classOf[WorkerCommunicationServlet],"/worker/*")
    server.setHandler(context)
    val n = new Thread(WorkerPoll)
    n.start()
    server.start()
    server.join()
  }
  run()
}