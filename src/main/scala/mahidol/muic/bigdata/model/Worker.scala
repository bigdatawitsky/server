package mahidol.muic.bigdata.model
import java.security.MessageDigest

import org.apache.commons.validator.routines.InetAddressValidator
import mahidol.muic.bigdata.enumeration.WorkerStatus

//Future, change the exception
case class Worker(ip: String, port: Int, var status: WorkerStatus.Status) extends  Comparable[Worker]{
//  require(port > 0 && port < 65535, throw new Exception)
//  require(InetAddressValidator.getInstance().isValidInet4Address(ip), throw new Exception)

  override def compareTo(o: Worker): Int = {
    if(this.ip == o.ip && this.port == o.port){
      return 0
    }
    return -1
  }
}
