package mahidol.muic.bigdata.model

import java.security.MessageDigest
import mahidol.muic.bigdata.enumeration.JobStatus
case class Job(target: String, var status: JobStatus.Status) extends Comparable[Job]{
//  val jobHash = MessageDigest.getInstance("MD5").digest(target.getBytes).toString
  val jobHash = target
  var answer:String  = null;

  def getAnswer(): String ={
    if (status != JobStatus.completed)
      return null
    answer
  }
  def setAnswer(answer: String) = {
    if(status != JobStatus.completed){
      this.answer = answer
      status = JobStatus.completed
    }
  }


  override def compareTo(o: Job): Int = {
    if(this.target == o.target){
      return 0
    }
    return -1
  }

  override def toString: String = s"Job: target=${target} status=${status.toString}"
}
