package mahidol.muic.bigdata.model
import org.json4s._
import org.json4s.jackson.JsonMethods._
import org.json4s.JsonDSL._
import java.security.MessageDigest
import mahidol.muic.bigdata.enumeration.JobStatus


case class MiniJob(start: String, end: String, target: String, var status: JobStatus.Status){
  def toJson(): Unit ={
    val json = {"miniJob" -> {
      ("start" -> start) ~
        ("end" -> end) ~
        ("target" -> target) ~
        ("status" -> status.toString)
    }}
    compact(render(json))
  }

  override def toString: String = return s"Minijob: start=${this.start} end=${this.end} target=${this.target} status=${this.status.toString}"
}
