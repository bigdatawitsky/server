package mahidol.muic.bigdata.enumeration

object JobStatus extends Enumeration  {
  type Status =  Value
  val inProgress = Value("inprogress")
  val completed = Value("completed")
  val inQueue = Value("inqueue")
  val stopped = Value("stopped")
  val unknown = Value("unknown")
}
