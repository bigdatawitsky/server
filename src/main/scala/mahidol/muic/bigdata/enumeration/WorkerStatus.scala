package mahidol.muic.bigdata.enumeration

object WorkerStatus extends Enumeration{
  type Status = Value
  val busy = Value("busy")
  val unreachable = Value("unreachable")
  val free = Value("free")
  val completed = Value("completed")
}
