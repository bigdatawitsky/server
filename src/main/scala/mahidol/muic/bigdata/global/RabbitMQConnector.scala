package mahidol.muic.bigdata.global
import akka.actor.{ActorRef, ActorSystem}
import com.newmotion.akka.rabbitmq._
import mahidol.muic.bigdata.model.Job
import mahidol.muic.bigdata.model.MiniJob
import org.json4s._
import org.json4s.jackson.JsonMethods._
import org.json4s.JsonDSL._
import scala.io.Source


object RabbitMQConnector {
  implicit val system = ActorSystem()
  val file = Source.fromResource("config.conf").getLines().toArray.map(str => {
    val splt = str.split("=")
    splt(0) -> splt(1)
  }).toMap


  val factory = new ConnectionFactory()
  factory.setHost(file("RABBITMQ_HOST"))
  factory.setPort(file("RABBITMQ_PORT").toInt)


  val connection = system.actorOf(ConnectionActor.props(factory),"rabbitmq")
  val exchange = "amq.fanout"
  def setupPublisher(channel: Channel, self: ActorRef): Unit = {
    channel.queueDeclare("sky", true, false, false, null)
  }
  connection ! CreateChannel(ChannelActor.props(setupPublisher),Some("publisher"))


  val publisher = system.actorSelection("/user/rabbitmq/publisher")
  def sendMiniJobToRMQ(miniJob: MiniJob): Unit = {
    val mj = ("startString" -> miniJob.start) ~ ("endString" -> miniJob.end) ~ ("jobId" -> miniJob.target) ~ ("hash" -> miniJob.target)
    val rendered = compact(render(mj))
    def publish(channel: Channel): Unit ={
      channel.basicPublish("","sky",null,rendered.getBytes())
    }
    publisher ! ChannelMessage(publish, dropIfNoChannel = false)
  }
}
