package mahidol.muic.bigdata.global

import scala.concurrent.Promise
import org.apache.http.client.methods.{CloseableHttpResponse, HttpGet, HttpPost, HttpRequestBase}
import org.apache.http.impl.client.CloseableHttpClient
import java.util.concurrent.{ExecutorService, Executors, FutureTask}
import java.util.Date

import mahidol.muic.bigdata.model.Worker
import mahidol.muic.bigdata.global.WorkerTracker
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClientBuilder
import java.io.IOException

import org.json4s._
import org.json4s.JsonDSL._
import org.json4s.jackson.JsonMethods._
import mahidol.muic.bigdata.enumeration.{JobStatus, WorkerStatus}
import org.apache.commons.io.IOUtils
import org.json4s._
import org.json4s.jackson._
import org.apache.http.client.config.RequestConfig
import org.apache.http.entity.{ContentType, StringEntity}
import org.json4s.jackson.JsonMethods.parse
import org.json4s.jackson.Serialization


/**
  * what this class do is the following
  * 1) Use the worker from the WorkerTracker
  * 2) every n seconds send a http req to it as a future (and thus promise)
  * 3) the promise will be the http response from each worker
  * 4) if the response is bad, increment the ping of that worker in the WorkerTracker, also timeout to roughly 5-10s.
  * 5) if the response is ok, set the ping of that worker in the WorkerTracker to 0
  */
object WorkerPoll extends Runnable{
  implicit val formats = DefaultFormats
  val pool: ExecutorService = Executors.newFixedThreadPool(5)
  val timeout = 5000
  override def run(): Unit = {
    var pt = System.currentTimeMillis()
    val length = 10000 //10s
    while (true){
      println("Worker Poll Thread: sending out checks")
      if (pt + length < System.currentTimeMillis()){
        pt = System.currentTimeMillis()
        pingAllWorkers()
        println(s"Pinged all workers. Worker Count: ${WorkerTracker.workerPing.size} workers")
      }else{
        Thread.sleep(length)
      }
    }
  }

  /**
    * Sending param {global: Map[String,String]}
    * Receiving param (global: Map[String,String])
    */
  def pingAllWorkers(): Unit ={
    WorkerTracker.workerPing.keySet.par.foreach(worker => {
      println(s"Pinging worker: ${worker.ip + ":" + worker.port}")
      val requestBuilder = RequestConfig.custom()
      requestBuilder.setConnectionRequestTimeout(timeout)
      requestBuilder.setConnectTimeout(timeout)
      val client = HttpClientBuilder.create().setDefaultRequestConfig(requestBuilder.build()).build()
      val url = "http://" + worker.ip + ":" + worker.port + "/ping"
      try{
        println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
         println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
        val map = Map("global"->JobTracker.getGlobalMap())
        val sendingGlobalJson = Serialization.write(map)
        val httpPost = new HttpPost(url)
        httpPost.setHeader("Content-Type", "application/json")
        httpPost.setEntity(new StringEntity(sendingGlobalJson, ContentType.APPLICATION_JSON))
        val httpResponse = client.execute(httpPost)
        val entity = httpResponse.getEntity
        val encoding = "UTF-8"
        val body = IOUtils.toString(httpResponse.getEntity.getContent,encoding)
        println(s"Received back from ${worker.ip + ":" + worker.port}")
        if(httpResponse.getStatusLine.getStatusCode == 200){
          println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
          println(entity.getContent.toString)
          println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
          val jsonString = parse(body).extract[Map[String,Map[String,String]]]
          println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
          JobTracker.updateJobMap(jsonString("global"))
          println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
        }else{
          WorkerTracker.updateWorkerPing(worker)
        }
      }catch{
        case ex => {
          WorkerTracker.updateWorkerPing(worker)
          ex.printStackTrace()
          println("Logging: Something went wrong with worker")
        }
      }

    })
  }
}