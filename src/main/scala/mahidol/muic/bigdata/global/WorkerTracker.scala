package mahidol.muic.bigdata.global
import scala.collection.mutable
import mahidol.muic.bigdata.model.{Worker, MiniJob}
import java.util.concurrent.ConcurrentHashMap
import mahidol.muic.bigdata.enumeration.JobStatus
import mahidol.muic.bigdata.enumeration.WorkerStatus
import scala.collection.parallel.mutable.ParHashMap


object WorkerTracker {
  val workerJobMap = new ParHashMap[Worker,MiniJob]()
  val workerPing = new ParHashMap[Worker,Int]()
  val timeOutPing = 20

  def addWorker(worker: Worker): Boolean = {
    if(workerJobMap.keySet.contains(worker)){
      return false
    }
    setWorkerStatus(worker,WorkerStatus.free)
    workerJobMap.put(worker,null)
    workerPing += (worker -> 0)
    true
  }

  def removeWorker(worker: Worker): Unit = {
    workerJobMap.remove(worker)
    workerPing.remove(worker)
  }

  def updateWorkerPing(worker: Worker): Unit ={
    workerPing.put(worker,workerPing(worker)+1)
    if(workerPing(worker) == timeOutPing){
      unableToReachWorker(worker)
    }
  }

  def setWorkerStatus(worker: Worker, status: WorkerStatus.Status): Unit = {
    worker.status = status
  }

  def giveWorkerJob(worker: Worker,start: String, end: String, jh: String): Unit = {
    val m: MiniJob = MiniJob(start,end,jh,JobStatus.inProgress)
    giveWorkerJob(worker,m)
  }

  def giveWorkerJob(worker: Worker, job: MiniJob): Unit = {
    workerJobMap.put(worker,job)
    setWorkerStatus(worker,WorkerStatus.busy)
    workerPing.put(worker,0)
  }

  def freeWorkerJob(worker: Worker): MiniJob = {
    if (workerJobMap.contains(worker)){
      val j = workerJobMap(worker)
      workerJobMap.put(worker,null)
      setWorkerStatus(worker,WorkerStatus.free)
      return j
    }
    null
  }

  //used when the worker reach a certain ping, remove worker and add their job back into the rabbit mq
  def unableToReachWorker(worker: Worker): Unit = {
    if (workerJobMap.contains(worker))
      JobTracker.addJobIntoJobQueue(workerJobMap(worker))
    setWorkerStatus(worker,WorkerStatus.unreachable)
    val mj = freeWorkerJob(worker)
    removeWorker(worker)
    if (mj != null)
      JobTracker.addJobIntoJobQueue(mj)
  }
}
