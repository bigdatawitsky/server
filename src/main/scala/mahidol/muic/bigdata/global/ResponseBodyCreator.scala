package mahidol.muic.bigdata.global
import akka.actor.FSM.Failure
import org.json4s._
import org.json4s.jackson.JsonMethods._
import org.json4s.JsonDSL._
import mahidol.muic.bigdata.enumeration.JobStatus

object ResponseBodyCreator {
  def jobDispatcherStartSuccessReply(): String ={
    val json = "status" -> "acknowledged"
    compact(render(json))
  }
  def jobDispatcherStartFailureReply(failureReason: String): String ={
    val json = ("status" -> "failure") ~ ("reason" -> failureReason)
    compact(render(json))
  }
  def jobDispatcherStopSuccessReply(): String = {
    val json = "status" -> "acknowledged"
    compact(render(json))
  }

  def jobDispatcherStopFailureReply(failureReason: String): String = {
    val json = ("status" -> "failure") ~ ("reason" -> failureReason)
    compact(render(json))
  }

  def jobDispatcherGetSuccessReply(status: JobStatus.Value, answer: String): String = {
    val json = "status" -> status.toString
    if (status != JobStatus.completed){
      compact(render(json))
    }else{
      compact(render(json ~ ("answer" -> answer)))
    }
  }

  def jobDispatcherGetFailureReply(failureReason: String): String = {
    val json = ("status" -> "failure") ~ ("reason" -> failureReason)
    compact(render(json))
  }
  def workerDispatcherJoinSuccessReply(): String = {
    val json = "status" -> "acknowledged"
    compact(render(json))
  }
  def workerDispatcherJoinFailureReply(failureReason: String): String = {
    val json = ("status" -> "failure") ~ ("reason" -> failureReason)
    compact(render(json))
  }
  def workerDispatcherLeaveSuccessReply(): String = {
    val json = ("status" -> "acknowledged") ~ ("ip" -> RabbitMQConnector.factory.getHost) ~ ("port" -> RabbitMQConnector.factory.getPort)
    compact(render(json))
  }
}
