package mahidol.muic.bigdata.global

import mahidol.muic.bigdata.model.{Job, MiniJob}
import mahidol.muic.bigdata.enumeration.JobStatus
import mahidol.muic.bigdata.threads.IntervalPushThread

import scala.collection.parallel.mutable.ParHashMap
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

/**
  * Job = the entire job, job is basically job splitted into several parts for worker to process
  * miniJob = one single job for a worker to work on
  *
  */
object JobTracker {
  //contains current hash pointing to the job --> your global hash map
  val jobMap = new ParHashMap[String, Job]()

  def getGlobalMap(): ParHashMap[String,String] ={
    jobMap.map(tuple => {
      tuple._1 -> tuple._2.answer
    }).filter(tuple => {
      tuple._2 != null
    })
  }

  def updateJobMap(map: Map[String,String]): Unit = {
    val gm = getGlobalMap()
    val diff = gm.keySet.diff(map.keySet)
    diff.foreach(key => {
      if(map.contains(key)){
        val j = jobMap(key)
        j.answer = map(key)
        j.status = JobStatus.completed
        jobMap.put(key,j)
      }
    })
  }

  //Make this into a thread and execute it.
  def intervalCreation(hash: String): Unit= {
    val allItem = (Vector(' ') ++ ('A' to 'Z').toVector ++ ('a' to 'z').toVector ++ ('0' to '9').toVector).sliding(2).map(item => {
      item.head -> item.tail.head
    }).toMap
    addJobIntoJobQueue(MiniJob("","999",hash,JobStatus.inQueue))
    var layer8 = ' '
    var layer7 = ' '
    var layer6 = ' '
    var layer5 = ' '
    var layer4 = 'A'
    var start = ""
    var end = ""
    var j: MiniJob = null
    while(layer8 != '9'){
      while (layer7 != '9'){
        while (layer6 != '9'){
          while (layer5 != '9'){
            while (layer4 != '9'){
              start = layer8.toString + layer7.toString + layer6.toString + layer5.toString + layer4.toString + "999"
              layer4 = allItem(layer4)
              end = layer8.toString + layer7.toString + layer6.toString + layer5.toString + layer4.toString + "999"
              j = MiniJob(start.trim,end.trim,hash,JobStatus.inQueue)
              addJobIntoJobQueue(j)
            }
            start = layer8 + layer7 + layer6 + layer5 + layer4 + "999"
            layer4 = 'A'
            layer5 = allItem(layer5)
            end = layer8 + layer7 + layer6 + layer5 + layer4 + "999"
            j = MiniJob(start.trim,end.trim,hash,JobStatus.inQueue)
            addJobIntoJobQueue(j)
          }
          start = layer8 + layer7 + layer6 + layer5 + layer4 + "999"
          layer5 = 'A'
          layer6 = allItem(layer6)
          end = layer8 + layer7 + layer6 + layer5 + layer4 + "999"
          j = MiniJob(start.trim,end.trim,hash,JobStatus.inQueue)
          addJobIntoJobQueue(j)
        }
        start = layer8 + layer7 + layer6 + layer5 + layer4 + "999"
        layer6 = 'A'
        layer7 = allItem(layer7)
        end = layer8 + layer7 + layer6 + layer5 + layer4 + "999"
        j = MiniJob(start.trim,end.trim,hash,JobStatus.inQueue)
        addJobIntoJobQueue(j)
      }
      start = layer8 + layer7 + layer6 + layer5 + layer4 + "999"
      layer7 = 'A'
      layer8 = allItem(layer8)
      end = layer8 + layer7 + layer6 + layer5 + layer4 + "999"
      j = MiniJob(start.trim,end.trim,hash,JobStatus.inQueue)
      addJobIntoJobQueue(j)
    }
  }
//  intervalCreation("hello")

  def sendIntervalIntoQueue(inv: Vector[(String,String)], hash: String): Unit ={
    println("go into it")
    inv.foreach(item => addJobIntoJobQueue(MiniJob(item._1,item._2,hash,JobStatus.inQueue)))
  }

  /**
    * create new job,
    * thorw the hash into new intervalpushthread to create intervals and run into add job queue
    * do not join the thread because it takes a long ass time
    * @param hash
    */
  def createJob(hash: String): Unit = {
    val job: Job  = Job(hash, JobStatus.inQueue)
    jobMap.put(job.jobHash,job)
    val f = Future{
      intervalCreation(hash)
    }
    f.onComplete{case _ => println(s"${job.jobHash}'s intervals has been completed and sent to rabbitmq.")}
  }

  def getJobStatus(hash: String): (JobStatus.Value,String) = {
    val job = jobMap(hash)
    (job.status,job.getAnswer())
  }

  def containJobhash(hash: String): Boolean = {

    jobMap.keySet.contains(hash)
  }

  //we gonna use rabbit mq instead so that's probably another worker
  def addJobIntoJobQueue(miniJob: MiniJob): Unit = {
//    println(s"${miniJob.start} + ${miniJob.end}")
    RabbitMQConnector.sendMiniJobToRMQ(miniJob)
  }
}
