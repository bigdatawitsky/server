package mahidol.muic.bigdata.servlet
import mahidol.muic.bigdata.global.WorkerTracker
import org.scalatra._
import org.scalatra.ScalatraServlet
import org.json4s._
import org.json4s.JsonDSL._
import org.json4s.jackson.JsonMethods._
import mahidol.muic.bigdata.model.Worker
import mahidol.muic.bigdata.enumeration.WorkerStatus
import mahidol.muic.bigdata.global.ResponseBodyCreator

class WorkerCommunicationServlet extends ScalatraServlet{
  implicit val formats = DefaultFormats
  options("/*"){
    response.setHeader(
      "Access-Control-Allow-Headers", request.getHeader("Access-Control-Request-Headers"));
  }

  before("/worker/*"){
    try{
      val jsonString = parse(request.body)
      val extracted = jsonString.extract[Map[String,String]]
      if(!extracted.keySet.contains("ip") ||  !extracted.keySet.contains("port")){
        throw new Exception
      }
    }catch {
      case ex: Exception => {
        ex.printStackTrace()
        halt(403,"ip or port is not provided")
      }
    }
  }

  post("/join"){
    println("O haiyo joining desu")
    val jsonString = parse(request.body).extract[Map[String,String]]
    println(s"ip: ${jsonString("ip")} port:${jsonString("port")}")
    val worker = Worker(jsonString("ip"),jsonString("port").toInt,WorkerStatus.free)
    println("made worker, adding worker")
    val result = WorkerTracker.addWorker(worker)
    println(s"result of adding worker: ${result }")
    result match {
      case true =>  ResponseBodyCreator.workerDispatcherJoinSuccessReply()
      case false => ResponseBodyCreator.workerDispatcherJoinFailureReply("duplicate worker trying to join.")
    }
  }
  post("/leave"){
    val jsonString = parse(request.body).extract[Map[String,String]]
    val worker = Worker(jsonString("ip"),jsonString("port").toInt,WorkerStatus.free)
    WorkerTracker.freeWorkerJob(worker)
    WorkerTracker.removeWorker(worker)
    ResponseBodyCreator.workerDispatcherLeaveSuccessReply()
  }
}
