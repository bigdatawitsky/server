package mahidol.muic.bigdata.servlet

import mahidol.muic.bigdata.enumeration.JobStatus
import mahidol.muic.bigdata.global.{JobTracker, ResponseBodyCreator}
import org.apache.commons.lang3.concurrent.ConcurrentException
import org.json4s.DefaultFormats
import org.json4s.jackson.JsonMethods.parse
import org.scalatra._
import org.scalatra.halt

class JobDispatcherServlet extends ScalatraServlet {
  implicit val formats = DefaultFormats
  options("/*"){
    response.setHeader(
      "Access-Control-Allow-Headers", request.getHeader("Access-Control-Request-Headers"));
  }

  before("/job/*"){

    try{
      val jsonString = parse(request.body)
      val extracted = jsonString.extract[Map[String,String]]
      if(!extracted.keySet.contains("hash")){
        throw new Exception
      }
    }catch {
      case ex: Exception => {
        ex.printStackTrace()
        halt(403,"ip or port is not provided")
      }
    }
  }

  /**
    * params: hash
    * 1) take out the hash
    * 2) Create jobs (which in turn create minijobs) and put them into the queue
    * 3) if successful, return successful.
    *
    * Condition to creating new job
    * 1) if job hash already have it then sure, no need to create new one, send success reply and wait for the guy to get the status and
    * thats when we give back the actual password.
    * 2) if job hash does not have it, create new one, return the ack
    *
    * TODO: CONNECT TO RABBIT MQ AND THROW IN THE JOBS
    *
    */
  post("/start") {
    println("starting sth")
    val jsonString = parse(request.body)
    val extracted = jsonString.extract[Map[String,String]]
    val hash = extracted("hash")
    var pass = true
    var reason = ""
    println(s"took stuffs: hash: ${hash}")
    if(!JobTracker.containJobhash(hash)){
      println("Checked pass")
      JobTracker.createJob(hash)
    }
//    if (!JobTracker.jobMap.contains(hash)){
//      try{
//
//      }catch{
//        case ex: ConcurrentException =>
//          pass = false
//          reason = "Concurrent Exception"
//          ex.printStackTrace()
//        case ex: Exception =>
//          pass = false
//          reason = "Unknown Exception"
//          ex.printStackTrace()
//      }
//    }
    println(s"pass: ${pass}")
    println(s"returning item: ${reason}")
    if (pass)
      ResponseBodyCreator.jobDispatcherStartSuccessReply()
    else
      ResponseBodyCreator.jobDispatcherGetFailureReply(reason)

  }

  /**
    * params: hash
    * 1) stopped the job if job exist
    *   1.1) If job doesnt exist, give failure
    *   1.2) if job exists, give success and stopped the job, JobTracker function has been done
    *
    *  TODO: TELL THE WORKER TO STOP THE JOBS (FROM WORKER TRACKER) --> in fact there's no need if you dont want to, just
    *  let the worker finish the job if you want. need to talk to sky
    */
//  post("/stop") {
//    val jsonString = parse(request.body)
//    val extracted = jsonString.extract[Map[String,String]]
//    val hash = extracted("hash")
//    var pass = true
//    var reason = ""
//    if(JobTracker.jobMap.contains(hash)){
//      try{
//        JobTracker.stopJob(hash)
//      }catch {
//        case ex: Exception =>
//          pass = false
//          reason = "Unknown Exception"
//          ex.printStackTrace()
//      }
//    }else{
//      pass = false
//      reason = "Unable to find the specified job,"
//    }
//    if (pass)
//      ResponseBodyCreator.jobDispatcherStopSuccessReply()
//    else
//      ResponseBodyCreator.jobDispatcherStopFailureReply(reason)
//  }

  /**
    * 1) use the hash to get the job status,
    * 2) job has a getAnswer function which allows you to get answer if the job is completed, otheriwse it wiill return a null
    */
  post("/get"){
    println("This is get servlet")
    val jsonString = parse(request.body)
    val extracted = jsonString.extract[Map[String,String]]
    val hash = extracted("hash")
    var pass = true
    var reason = ""
    var status = JobStatus.unknown
    var answer = ""
    if(!JobTracker.jobMap.contains(hash)){
      pass = false
      reason = "Unable to find the specified job,"
    }else{
      val (s1,s2) = JobTracker.getJobStatus(hash)
      status = s1
      answer = s2
    }
//    println(s"Result from get servlet: pass=${pass}, reason=${reason}, status=${status.toString}, answer=${answer}")
    if (pass)
      ResponseBodyCreator.jobDispatcherGetSuccessReply(status,answer)
    else
      ResponseBodyCreator.jobDispatcherGetFailureReply(reason)
  }

  after(){
    response.addHeader("Content-Type","application/json")
  }
}
