package mahidol.muic.bigdata.threads

import mahidol.muic.bigdata.global.JobTracker

class IntervalPushThread(hash: String) extends Runnable{
  override def run(): Unit = {
    JobTracker.intervalCreation(hash)
  }
}
