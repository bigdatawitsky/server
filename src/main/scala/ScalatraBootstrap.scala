import mahidol.muic.bigdata._
import org.scalatra._
import javax.servlet.ServletContext

import mahidol.muic.bigdata.global.WorkerPoll
import mahidol.muic.bigdata.servlet.{JobDispatcherServlet, WorkerCommunicationServlet}

class ScalatraBootstrap extends LifeCycle {
  override def init(context: ServletContext) {
    context.mount(new JobDispatcherServlet, "/job/*")
    context.mount(new WorkerCommunicationServlet, "/worker/*")
    val n = new Thread(WorkerPoll)
    n.start()
  }
}
