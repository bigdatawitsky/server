package mahidol.muic.bigdata

import mahidol.muic.bigdata.servlet.JobDispatcherServlet
import org.scalatra.test.scalatest._

class MiniJobDispatcherServletTests extends ScalatraFunSuite {

  addServlet(classOf[JobDispatcherServlet], "/*")

  test("GET / on DispatcherServlet should return status 200"){
    get("/"){
      status should equal (200)
    }
  }

}
